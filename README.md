# zim-sddm-theme

Screenshot:

![zim-sddm-theme](/screenshot.png)

## Getting started

```
git clone https://gitlab.com/jasonrob/zim-sddm-theme.git
sudo cp -r zim-sddm-theme /usr/share/sddm/themes
```

Edit the `Current` var in the `[Theme]` stanza in `/etc/sddm.conf`:

```
[Theme]
Current=zim-sddm-theme
```

# Credit

All credit goes to as I just bastardized their fine work:

[https://github.com/simonesestito/dark-arch-sddm](https://github.com/simonesestito/dark-arch-sddm)
